package com.example.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication/*(scanBasePackages = {"org.jeecg.modules.jmreport"})*/
@ComponentScan({"org.jeecg.modules.jmreport","com.example.report"})
public class ReportApplication {
    public static Map<String,Object> map=new HashMap<>();

    public static void main(String[] args) {
        SpringApplication.run(ReportApplication.class, args);
    }

}
